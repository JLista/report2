'use strict';

/**
 * @ngdoc function
 * @name interviewProjectApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the interviewProjectApp
 */
angular.module('interviewProjectApp')
  .controller('MainCtrl', function ($scope, PrintService, $window) {

    // The model to use
    $scope.model = {
      deliverTo: 'owner', // Default value
      instructions: '',
      documents: {
        files: []
      }
    };

    // Get the documents
    PrintService.getDocuments()
      .then(function (response) {
        // We need to track print/color/notes for each filename; lets build the model as soon as the data comes back
        // from the service
        response.documents.filename.forEach(function forEach(value) {
          var document = {
            name: value,
            print: true,
            color: false,
            notes: ''
          };

          // Currently, the only way to tell if a document is not supported is by looking at the last character and
          // determining if the name ends with '*'
          if ('*' === document.name.substring(document.name.length-1)) {
            document.print = false;
            document.disabled = true;
          }

          $scope.model.documents.files.push(document);
        });

      });

    $scope.submit = function submit() {
      // Submit the print job
      var printJob = {
        printRequest: {
          deliverTo: $scope.model.deliverTo,
          instructions: $scope.model.instructions,
          documents: []
        }
      };

      $scope.model.documents.files.forEach(function forEach(document) {
        // Sample format of parameter
        //
        //  {
        //    "printRequest": {
        //      "deliverTo": "owner",
        //      "instructions": "Please do not staple together any documents in this print job",
        //      "documents": {
        //        "document": [
        //          {
        //            "filename": "agenda.pdf",
        //            "color": "false",
        //            "notes": "print on high-quality paper stock"
        //          },
        //          {
        //            "filename": "Important Diagram.jpg",
        //            "color": "true"
        //          },
        //          {
        //            "filename": "Meeting Notes.docx",
        //            "color": "false"
        //          }
        //        ]
        //      }
        //    }

        // Build a document descriptor
        var documentDescriptor = {
          filename: document.name,
          color: document.color
        };

        // Only process the document if 'print' is true
        if (!!document.print) {
          // Only add 'notes' if they are present on the document
          if (!!document.notes) {
            documentDescriptor.notes = document.notes;
          }
          printJob.printRequest.documents.push(documentDescriptor);
        }
      });

      // Submit the job
      PrintService.submitPrintJob(printJob)
        .then(function (response) {
          // Forward
          $window.location.href = 'invite-sent.html?success=true&message=The print job was submitted with ID ' + response.requestConfirmation.printJobId + '!';
        });
    };

    $scope.cancel = function cancel() {
      // Forward
      $window.location.href = 'invite-sent.html?success=false&message=The print job was not submitted :(';
    };

  });
