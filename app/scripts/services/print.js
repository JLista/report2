'use strict';

/**
 * @ngdoc service
 * @name interviewProjectApp.Print
 * @description
 * # Print
 * Service in the interviewProjectApp.
 */
angular.module('interviewProjectApp')
  .service('PrintService', function ($q, $http, $location) {

    // The API root
    var apiRoot = $location.search().endpoint, // 'http://demo2177959.mockable.io',
      // Gets the documents
      getDocuments = function getDocuments() {
        if (!!apiRoot) {
          return $http({
            method: 'get',
            url: apiRoot + '/apis/print_center/documents'
          })
            .then(function (data) {
              return data.data;
            });
        } else {
          return $q.resolve(
            // JSON response
            //
            // {
            //   "documents": {
            //     "filename": [
            //       "agenda.pdf",
            //       "Diagram_data.xlsx",
            //       "Important Diagram.jpg",
            //       "Meeting Notes.docx",
            //       "newCode.zip"
            //     ]
            //   }
            // }
            {
              'documents': {
                'filename': [
                  'agenda.pdf',
                  'Diagram_data.xlsx',
                  'Important Diagram.jpg',
                  'Meeting Notes.docx',
                  'newCode.zip'
                ]
              }
            }
          );
        }
    },
      submitPrintJob = function submitPrintJob(printJob) {
        if (!!apiRoot) {
          return $http({
            method: 'post',
            data: printJob,
            url: apiRoot + '/apis/print_center/print_jobs'
          })
            .then(function (data) {
              return data.data;
            });
        } else {
          return $q.resolve(
            {
              'requestConfirmation': { 'printJobId': '987654321' }
            }
          );
        }
      };

    // Return the content
    return {
      getDocuments: getDocuments,
      submitPrintJob: submitPrintJob
    };

  });
