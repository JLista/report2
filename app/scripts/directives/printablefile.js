'use strict';

/**
 * @ngdoc directive
 * @name interviewProjectApp.directive:PrintableFile
 * @description
 * # PrintableFile
 */
angular.module('interviewProjectApp')
  .directive('printableFile', function () {
    return {
      template: '<tr ng-class="{disabled: file.disabled}"><td>{{file.name}}</td><td><div class="checkbox"><label><input type="checkbox" ng-model="file.print" ng-disabled="file.disabled"></label></div></td><td><div class="checkbox"><label><input type="checkbox" ng-model="file.color" ng-disabled="file.disabled"></label></div></td><td><input type="text" class="form-control" placeholder="Notes" ng-model="file.notes" ng-disabled="file.disabled"></td></tr>',
      replace: true,
      restrict: 'A'
    };
  });
