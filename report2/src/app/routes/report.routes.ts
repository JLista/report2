import { Routes } from "@angular/router";

import { ReportDetailComponent } from "../report-detail/report-detail.component";
import { ReportListComponent } from "../report-list.component";

import { REPORT_DETAIL_ROUTES } from "../routes/report-detail.routes";

export const REPORT_ROUTES: Routes = [

    { path: '', component: ReportListComponent},
    { path: ':id', component: ReportDetailComponent, children: REPORT_DETAIL_ROUTES },

];