import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { routing } from "./routes/app.routing";

@Component({
  selector: 'sr-login',

  templateUrl: 'login.component.html',
  
  styleUrls: ['./stylesheets/login.component.css',
              './stylesheets/global.css']
})
export class LoginComponent {

  public login(){
    this._router.navigate(['/reports']);
  }

  public constructor(private _router: Router) {}

}
