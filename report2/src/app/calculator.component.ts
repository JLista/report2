import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { CalculatorService } from './services/calculator.service';
import { Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'sr-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./stylesheets/calculator.component.css',
              './stylesheets/side-nav.css' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalculatorComponent implements OnInit {

  private operand: string = "0";
  private queue: any[] = [];
  private nodePrototype = {operand: '',
                            unary: '',
                            binary: ''};
  
  private stackIsCalculated: boolean = true;
  private operandHasPoint: boolean =  false;


 
  constructor(private calculatorService: CalculatorService) { }

  ngOnInit() {
  }

  private clearOperand(){

    this.operand = "0";
    this.operandHasPoint = false;
  }

  private clearStack(){

    this.queue = [];
    this.operandHasPoint = false;
  }

  private clearAll(){

    this.clearOperand();
    this.clearStack();
  }


  /*
   *name: setOperand
   *description: when a number key is pressed and we don't currently have an operand, the operand is set to that number.
   *             if we do have an operand, concatenate it to the end.
   */

  private setOperand(op: string){

    if (this.operand == "0" || this.operand == "" || this.stackIsCalculated){

      this.operand = op;
      this.stackIsCalculated = false;
    }
    else {

      this.operand = this.operand.concat(op);

    }

  }

  private setBinaryOperator(op: string){

    if (this.operand.substring(this.operand.length-1, this.operand.length) != "."){

      let operandToken = {operand: this.operand,
                          unary: '',
                          binary: ''
                          };

      let operatorToken = {operand: '',
                          unary: '',
                          binary: op};

      
      if (this.queue.length > 0 && this.queue[this.queue.length-1].binary != 0  && this.queue[this.queue.length-1].binary != op){

        this.queue[this.queue.length-1].binary = op;
        return;

      }

      if (this.queue.length == 0 || (this.queue.length > 0 && this.queue[this.queue.length-1].unary == "")){
                    
        this.queue.push(operandToken);

      }
      

      this.queue.push(operatorToken);
      
      this.stackIsCalculated = true;
      this.operandHasPoint = false;
      console.log(this.queue);

    }
  }

  private setUnaryOperator(op: string){

    if (this.operand.substring(this.operand.length-1, this.operand.length) != "."){

      let operandToken = {operand: this.operand,
                          unary: op,
                          binary: ''
                          };

      this.queue.push(operandToken);
      this.operand = this.roundAnswer(this.evaluateUnary(operandToken));
      this.operandHasPoint = false;
      this.stackIsCalculated = true;
      console.log(this.queue);

    }

  }

  /*
   *name: backspace
   *description: removes the last digit from the operand, unless it's a single digit, in which case it is set to 0
   */

  private backspace(){

    if (!this.stackIsCalculated){

      if (this.operand.length == 1){

        this.operand = "0";
      }
      else{
        
        if (this.operand.substring(this.operand.length-1, this.operand.length) == "."){

          this.operandHasPoint = false;
        }

        this.operand = this.operand.slice(0,this.operand.length-1);

      }
    }

  }

  /*
   *name: setDecimal
   *description: when decimal point key is pressed, adds a decimal point to the operand
   */

  private setDecimal(){

    //if operand already has decimal, we can't add another one

    if (this.operandHasPoint == false){

      if (this.operand == ""){

        this.operand = "0.";
      }
      else{

        this.operand = this.operand.concat(".");
      }
      this.operandHasPoint = true;
    }
  }  



  private evaluate(){
    

    if (this.queue.length > 0){

      for (var i = 0; i < this.queue.length; i++){

        this.queue[i].operand = this.evaluateUnary(this.queue[i]);
      }

      let op1 = this.queue.shift();

      let result = this.evaluateQueue(op1.operand,this.queue);

      console.log("done eval bin");

      this.stackIsCalculated = true;


      this.queue = [];

      this.operandHasPoint = false;

    }
  }

  private evaluateQueue(op: any, queue: any[]){


    console.log("evaluating");

    console.log(queue);
    console.log(op);
    

    if (queue.length == 0){

      console.log("done with queue");

      this.operand = this.roundAnswer(op);

      return op;
    }
    else if (queue.length == 1){

      console.log("getting operand");


      this.operand = this.roundAnswer(this.evaluateBinary(op,this.operand,queue[0]));


      return;

    
    }

    let operator = queue.shift();

    let operand = queue.shift();

    console.log("operator");
    console.log(operator);

    console.log("operand");
    console.log(operand);

    let result = this.evaluateBinary(op, operand.operand, operator);

    console.log("result " + result);

    this.evaluateQueue(result, queue);

  }

  private evaluateBinary(token1: string, token2: string, op: any){

    console.log("eval binary");

    let operand1 = Number(token1);
    let operand2 = Number(token2);
    let operator = op.binary;
    let result = "";

    console.log(operand1);
    console.log(operator);
    console.log(operand2);

    switch(operator){

      case "add":
        result = (operand1 + operand2).toString();
        break;
      case "sub":
        result = (operand1 - operand2).toString();
        break;
      case "mult":
        result = (operand1 * operand2).toString();
        break;
      case "div":
        result = (operand1 / operand2).toString();
        break;

    }

    console.log("got binary result:");
    console.log(result);
    return result;
    
  }

  private evaluateUnary(token: any){

      console.log("eval unary");
      console.log(token);

      let operand: number = Number(token.operand);
      let result = "";

      switch (token.unary){

        case "":
          result = operand.toString();
          break;
        case "sqrt":
          result = Math.sqrt(operand).toString();
          break;
        case "square":
          result = Math.pow(operand,2).toString();
          break;
        case "invAdd":
          result = ((-1)*operand).toString();
          break;
        case "invMul":
          result = (1/operand).toString();
          break;
        default:
          result = operand.toString();
          break;
      }
          
      console.log("got unary result");
      console.log(result);
      return result;
  }

  private roundAnswer(num: string){

    return (Math.round(10000*Number(num))/10000).toString();
  }

}


@Pipe({name: 'queueDisplay', pure: false})

export class QueueDisplayPipe implements PipeTransform {
  transform(queue: any[], param: any): string {
    

    let queueString: string = "";

    if (queue != []){

      for (var i = 0; i < queue.length; i++){

        let token = queue[i];

        if (token.unary != ""){


          switch (token.unary){

            case "invAdd":
              queueString = queueString.concat("(-" + token.operand + ") ");
              break;
            case "invMul":
              queueString = queueString.concat("(1/" + token.operand + ") ");
              break;
            case "sqrt":
              queueString = queueString.concat("√(" +  token.operand + ") ");
              break;
            case "square":

              let exp = "2";
              let expSup = exp.sup();
              queueString = queueString.concat("(" + token.operand + "^2) ");
              break;
            default:
              break;
          
          }


        }
        else if (token.binary != ""){

          var symbol = "";

          switch (token.binary){

            case "add":
              symbol = "+";
              break;
            case "sub":
              symbol = "-";
              break;
            case "mult":
              symbol = "*";
              break;
            case "div":
              symbol = "÷";
          }

          queueString = queueString.concat(symbol + " ");
        }
        else{

          queueString = queueString.concat(token.operand + " ");

        }
      }
    }

    return queueString;
    
  }
}

@Pipe({name: 'operandDisplay', pure: false})

export class OperandDisplayPipe implements PipeTransform {
  transform(operand: string, param: any): string {
    
    let value = Number(operand);
    return (Math.round(value * 10000)/10000).toString();
  }
}