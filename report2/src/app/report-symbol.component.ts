import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'apt-report-symbol',
  template: `
  
  
           <div class = "button" [ngStyle] = "{'background-color': background_color}" >
               <div class = "button--text" [ngStyle] = "{'font-size': font_size, 'top': top}"> {{text}} </div>
           </div>`,

  styles: [`.button {
                    width: 40px;  
                    height: 40px;  
                    border-radius: 3px;  
                    text-align: center;  
                    position: relative;  
                    right: 15%;
                    }`,
          `.button--text {
                    color: #fff;
                    letter-spacing: .25px;
                    font-family: roboto;
                    position: relative;
                    }`
          ]

})
export class ReportSymbolComponent implements OnInit {

  @Input() value: string;
  background_color: string;
  text: string;
  font_size: string;
  top: string;

  constructor() { }

  ngOnInit() {

    if (this.value == 'Training'){


      this.background_color = "#2f957e";
      this.text = "T";
      this.font_size = '24px';
      this.top = '10%';

    }
    else if (this.value == 'Team 360'){

      this.background_color = "#0b2d53";
      this.text = "360";
      this.font_size = '18px';
      this.top = '20%';


    }
    else if (this.value == 'Engagement'){

      this.background_color = "#a01c20";
      this.text = "E";
      this.font_size = '24px';
      this.top = '10%';

    }
  }
}
