/**
 * Name: Report Detail
 * 
 * Description: This component displays detail for some report which is specified by an ID. It contains a header, 
 *              a nav bar, and a display panel. The default view on the display panel is "Report Default". 
 */

import { Component, OnDestroy} from '@angular/core';
import { ActivatedRoute, Router} from "@angular/router"

import { Subscription } from "rxjs/Rx";

import { ReportsService } from "../services/reports.service";
import { Report} from "../report";

@Component({
  selector: 'sr-report-detail',

  templateUrl: './report-detail.component.html',

  styleUrls: ['../stylesheets/content-box.css',
              '../stylesheets/side-nav.css',
              '../stylesheets/nav-content-page.css',
              '../stylesheets/global.css']
})
export class ReportDetailComponent implements OnDestroy {
  
  private repIndex: number; 

  private respondentsGenerated = false; //flag will turn on when respondent list is initialized, triggering template to load

  private selectedReport: Report;  

  private respondents: String[] = [];

  private questions: any[] = [];

  private routeSubscription: Subscription;
  private reportSubscription: Subscription;
  private answerSubscription: Subscription;

  private expandedNav: boolean[] =[true,false,false,false,false]; //each bool represents one of the menu items
                                                                  //when they are true, that menu is expanded

  constructor(private route: ActivatedRoute, 
              private reportsService: ReportsService,
              private router: Router) {

    this.routeSubscription = this.route.params.subscribe(
      (params: any) => {

      this.repIndex = params['id']; //get the report ID from the router parameters
      
      this.reportsService.getReport(this.repIndex); //retrieve the report with that index

      this.reportSubscription = this.reportsService.gotReport.subscribe( //execute when report is sent from service
      (rep: Report) => { 

        this.selectedReport = rep;
        this.questions = this.selectedReport.questionJson;
        this.reportsService.getResponses(this.selectedReport.sgId);  //when report is retrieved, get responses for that report
 
      });
     
      this.answerSubscription = this.reportsService.gotSurveyAnswers.subscribe( answers=> { //execute when responses are retrieved

          for (var i = 0; i < answers.length; i++){

            this.respondents.push(answers[i]['[url("eeid")]']); //create list of names (stored under [url("eeid")]), this will change
                                                                //when we integrate with the backend

          }
          this.respondentsGenerated = true; //turn on flag to let template display, now that we have all of the info loaded
          this.answerSubscription.unsubscribe();

      });
     
    });
    
  }

  private toggleNav(index: number){ //when user clicks on a menu item, expand that menu item

    this.expandedNav[index] = this.expandedNav[index]? false:true;
  }

  ngOnDestroy() {

    this.routeSubscription.unsubscribe();
    this.reportSubscription.unsubscribe();
  }
}
