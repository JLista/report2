/**
 * Name: ReportDefault
 * Description: This is the component which is first displayed when a report is selected, and can be accessed
 *              through the "Survey Details" link on the nav. It displays basic information about a survey.
 */


import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { Subscription } from 'rxjs/Rx';

import { MenuModule, MenuItem } from 'primeng/primeng';

import { ReportsService } from '../services/reports.service';
import { Report } from "../report";

@Component({
  selector: 'sr-report-default',
  templateUrl: 'report-default.component.html',

  styleUrls: ['../stylesheets/content-box.css',
              '../stylesheets/global.css']

})
export class ReportDefaultComponent {

  private reportIndex: number;

  private selectedReport: any;

  private reportSubscription: Subscription;

  public constructor(private reportsService: ReportsService, 
                     private routes: ActivatedRoute) {
                    }


  ngOnInit(){

      this.reportIndex = +this.routes.snapshot['_urlSegment']['segments'][1]['path'];

      this.reportSubscription = this.reportsService.gotReport.subscribe(
      (rep: Report) => { 

        this.selectedReport = rep;
 
      });

      this.reportsService.getReport(this.reportIndex);
  }

   ngOnDestroy () {
     this.reportSubscription.unsubscribe();
  }
}
