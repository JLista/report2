import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';

import { MdSidenavModule,MdSidenav } from '@angular/material';

import { Subscription } from 'rxjs/Rx';

import { TableService } from './services/table.service';
import { CalculatorService } from './services/calculator.service';

import { SideNavComponent } from './side-nav.component';


@Component({
  selector: 'sr-reports',

  templateUrl: 'reports.component.html',

  styleUrls: ['./stylesheets/reports.component.css',
              './stylesheets/global.css']
})
export class ReportsComponent {

 
  private navOpened = false;
  private calcOpened = false;

  private navOpenedSubscription: Subscription;
  private calcOpenedSubscription: Subscription;
  private screenResizedSubscription: Subscription;
  private smallScreen: Boolean;


  private menuMode: string;

  @ViewChild('menu') menu: MdSidenav;
  @ViewChild('calc') calc: MdSidenav;

  public constructor(private tableService: TableService,
                     private calculatorService: CalculatorService) {}


  ngOnInit(){

        if (window.innerWidth < 900){  //start in single view mode if screen is small

            this.smallScreen = true;
            this.menuMode = "cover";
        }
        else{ //otherwise start in desktop mode

            this.smallScreen = false;
            this.menuMode = "side";
        }

        //if screen is resized, swtich view mode

        this.screenResizedSubscription = this.tableService.resizeScreenEmitter.subscribe((event) =>{

            var width = event.target.innerWidth; //get screen width


            if (width < 900 && !this.smallScreen){  //if screen was previously larger than 700 and is resized to smaller than 700

              this.smallScreen = true;
              this.menuMode = "cover";
            }
            else if (width >= 900 && this.smallScreen){

              this.smallScreen = false;
              this.menuMode = "side";
            }
           

        });

  }

  ngAfterViewInit(){

    this.navOpenedSubscription = this.tableService.toggleNavEmitter.subscribe((event)=>{

      this.menu.toggle();
      this.navOpened = this.navOpened? false:true;

      if (this.smallScreen && this.navOpened && this.calcOpened){

        this.calc.close();
      }

    });
    this.calcOpenedSubscription = this.calculatorService.calcDisplay.subscribe((event)=>{

      this.calc.toggle();
      
      this.calcOpened = this.calcOpened? false:true;
      if (this.smallScreen && this.calcOpened && this.navOpened){

        this.menu.close();
      }

    });
  }

  ngOnDestroy(){

    this.navOpenedSubscription.unsubscribe();
    this.screenResizedSubscription.unsubscribe();
  }

}
