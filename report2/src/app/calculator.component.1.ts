import { Component, OnInit } from '@angular/core';
import { CalculatorService } from './services/calculator.service';

@Component({
  selector: 'sr-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./stylesheets/calculator.component.css',
              './stylesheets/side-nav.css' ]
})
export class CalculatorComponent implements OnInit {

  private operations = "";
  private operand = "0";

  
  private progress: number = 0;

  private num1hasPoint = false;
  private num2hasPoint = false;

  constructor(private calculatorService: CalculatorService) { }

  ngOnInit() {
  }

  private append(input: string){

    if (this.progress == 0){

      if (/[0-9]/.test(input)){

          this.operations = this.operations.concat(input);
          this.progress = 1;

          console.log(this.progress);
      }
    }
    else if (this.progress == 1){

        if (/[\\.]/.test(input)){

          if (!this.num1hasPoint){

          this.num1hasPoint = true;

          }
          else{
            return;
          }
        }

        this.operations = this.operations.concat(input);

        if (!/[\\.]|[0-9]/.test(input)){
          this.progress = 2;

          console.log(this.progress);
        }
    }

    else if (this.progress == 2){

      if (/[0-9]/.test(input)){
        this.operations = this.operations.concat(input);
        this.progress = 3;
        console.log(this.progress);
      }

    }
    else if (this.progress == 3){

      if (/[\\.]/.test(input)){

          if (this.num2hasPoint){

            return;
        
          }
          else{
            this.num2hasPoint = true;
          }
          
        }
        if (/[\\.]|[0-9]/.test(input)){
          this.operations = this.operations.concat(input);
        }
    }

  }

  private backspace(){


    let removedToken = this.operations.substring(this.operations.length-1, this.operations.length);

    this.operations = this.operations.substring(0,this.operations.length-1);

    console.log("removed" + removedToken);
    console.log("current" + this.operations);

    if (!/[\\.]|[0-9]/.test(removedToken)){

      this.progress = 1;
    }
    else if (!/[\\.]|[0-9]/.test(this.operations.substring(this.operations.length-1,this.operations.length))){

      this.progress = 2;
    }
    else if (this.operations == ""){
      this.progress = 0;
    }
    else if (/[\\.]/.test(removedToken)){   

      if (this.progress < 3){

        this.num1hasPoint = false;
      }
      else{

        this.num2hasPoint = false;
      }
    }

    console.log("progress" + this.progress);
  }

  private clear(){

    this.operations = "";

    this.progress = 0;

    this.num1hasPoint = false;
    this.num2hasPoint = false;
  }

  private invert(){

    if (this.progress == 1){

      let answer = Number(this.operations);

      this.operations = (Math.round(answer * -1000) / 1000).toString();

    }
  }
  private sqrt(){

    if (this.progress == 1){

      let answer = Number(this.operations);

      this.operations = (Math.round(Math.sqrt(answer) * 1000) / 1000).toString();

    }
  }

  private closeCalc(){

    this.calculatorService.toggleCalc();
  }

  private evaluate(){

    if (this.progress == 3){

      var i = 0;

      var opIndex: number;

      var sign = 1;

      if (this.operations.substring(0,1) == "-"){

        this.operations = this.operations.substring(1,this.operations.length);
        sign = -1;
      }

      while (i < this.operations.length){


          let token = this.operations.substring(i,i+1);


          if (!/[\\.]|[0-9]/.test(token)){

            opIndex = i;
            break;
          }

          i++;

      }

      let num1 = Number(this.operations.substring(0,opIndex));
      let op = this.operations.substring(opIndex, opIndex+1);
      let num2 = Number(this.operations.substring(opIndex+1, this.operations.length));

      var answer: number;

      switch (op){

        case "+": 
          answer = sign*num1 + num2;
          break;
        case "-":
          answer = sign*num1 - num2;
          break;
        case "*":
          answer = sign*num1 * num2;
          break;
        case "/":
          answer = sign*num1 / num2;
          break;
        default:
          answer = 0;
          break;


      }

      this.operations = (Math.round(answer * 1000) / 1000).toString();
      this.progress = 1;
      this.num2hasPoint = false;
    
    }

  }


}
