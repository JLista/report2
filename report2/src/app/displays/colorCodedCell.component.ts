import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'color-cell',
 
  template: `

           <div [ngStyle] = "{'background-color': background_color}" align = "right">
                {{value}} %
           </div>
            
  `,
 
  styleUrls: ['../stylesheets/detailTable.css']
})

export class ColorCodedCell implements OnInit {

    @Input() value: number;
    background_color: string;
  
    constructor() {}

    ngOnInit(){

        //generate a HSL code for the value, where higher value means higher hue

        this.background_color = "hsl(" + this.value.toString() + ", 75%, 75%)";
    }
    
}
