import { Injectable, EventEmitter } from '@angular/core';

import { Observable, Subscription } from 'rxjs';

@Injectable()
export class CalculatorService {

    public calcDisplay = new EventEmitter<any>();

    public toggleCalc(){

        this.calcDisplay.emit();
        
    }
}