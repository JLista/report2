import { Injectable, EventEmitter } from '@angular/core';

import { Observable, Subscription } from 'rxjs';

@Injectable()
export class TableService {


  resizeScreenEmitter = new EventEmitter<any>();

  searchedReports = new EventEmitter<any>();

  toggleNavEmitter = new EventEmitter<any>();


  resizeScreen(event: Event){

    this.resizeScreenEmitter.emit(event);
    
  }

  search(event: Event){

    this.searchedReports.emit(event);

  }

  toggleNav(){

    this.toggleNavEmitter.emit();

  }

}