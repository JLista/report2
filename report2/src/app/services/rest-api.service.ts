import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";

import {Observable} from "rxjs/Rx";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';

@Injectable()
export class RestApiService {

  constructor(private http: Http) {}

    public fetchServlet(type: string, id: string): Observable<any>{
      
      let servletUrl: String = "http://10.0.0.56:8080/demo/welcome?";

      return this.http.get(servletUrl + "type=" + type + "&id=" + id)
        .map(this.extractData)
        .catch(this.handleError)
    }


  public fetch(url): Observable<any>{
      

    return this.http.get(this.getBaseUrl() + url)
      .delay(10)
      .map(this.extractData)
      .catch(this.handleError)
  }

  private getBaseUrl(){

    return "https://restapi.surveygizmo.com/v4/";    
  }

  private extractData(res:Response) {
    let body = res.json();


    if (body){
      return body.data || body
    } else {
      return {}
    }
  }

  private handleError(error:any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead

    return Observable.throw(errMsg);

  }

}


