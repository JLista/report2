export class Report{

  constructor(public reportName: string,  //name of survey
              public type: string,        //type of survey
              public teamName: string,    //name of team
              public client: string,      //name of client
              public status: string,      //status of team
              public date: string,        //survey start date
              public sgUrl: string,       //url to edit on SurveyGizmo
              public index: number,       //index of report (for routing to appropriate detail page)
              public responses: number,   //number of responses to survey
              public sgId: number,        //ID on SurveyGizmo (for pulling additional data from API)
              public questionJson: any,   //JSON object containing the survey's questions
              public selected: boolean    //true if checkbox is clicked
  ){}
}
