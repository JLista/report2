# interview-project

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Initial setup

Run `npm install` to install NPM deps and run `bower install` to install Bower deps.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.


Time Estimates:
* 5 minutes to scaffold code
 * Install deps
  * `npm install -g grunt-cli bower yo generator-karma generator-angular`
* Scaffold the project (accept most defaults, do not use compass)
 * `yo angular`
* 5 minutes to remove unnecessary markup
* 5 minutes to configure ide
* 5 minutes to stub out content
* 10 minutes to style stubbed out content
* 5 minutes to add service
 * `yo angular:service Print`
* Copied responses from the PowerPoint presentation
* 5 minutes to add REST mocks at www.mockable.io
 * http://demo2177959.mockable.io/apis/print_center/documents
 * http://demo2177959.mockable.io/apis/print_center/print_jobs
* 10 minutes to mock out success page (extra credit)
* 25 minutes looking up stuff such as styles (centering divs), Bootstrap API usage, etc.
* 5 minutes to create the directive
 * `yo angular:directive PrintableFile`
* 10 minutes to wrangle the correct directive template
* 30 minutes to wire in the service and format the data (incoming and outgoing)
* 20 minutes final touches (revisit styles, add some comments)