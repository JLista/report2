'use strict';

describe('Directive: PrintableFile', function () {

  // load the directive's module
  beforeEach(module('interviewProjectApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<-printable-file></-printable-file>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the PrintableFile directive');
  }));
});
