'use strict';

describe('Service: Print', function () {

  // load the service's module
  beforeEach(module('interviewProjectApp'));

  // instantiate service
  var Print;
  beforeEach(inject(function (_Print_) {
    Print = _Print_;
  }));

  it('should do something', function () {
    expect(!!Print).toBe(true);
  });

});
